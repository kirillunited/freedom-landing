$(document).ready(function () {


    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var dropMenu = $(".nav_drop_container");
            dropItem = $(".nav_drop");
            if (!dropMenu.is(e.target) && dropMenu.has(e.target).length === 0 && !dropItem.is(e.target) && dropItem.has(e.target).length === 0) {
                $('.nav_drop_container, .nav_drop').removeClass('active');
            }
        });
    });

    $("body").on("click", ".header_nav ul .nav_drop", function () {
        if ($(this).hasClass('active')) {
            $('.nav_drop_container, .nav_drop').removeClass('active');
        }
        else {
            $(this).addClass('active').siblings().removeClass('active');
            $('.nav_drop_container__block').children('.nav_drop_container').removeClass('active').eq($('.header_nav ul li.nav_drop').index(this)).addClass('active');
        }
    });

// BEGIN: 06-08-18
    $("body").on("click", ".nav_drop_nav-top ul li", function () {
        $(this).addClass('active').siblings().removeClass('active');
        // $('.nav_drop_nav-top_child-wrap').children('.nav_drop_nav-top_child-item').removeClass('active').eq($('.nav_drop_nav-top ul li').index(this)).addClass('active');
    });

    $("body").on("click", ".nav_drop_nav-top_child-item ul li", function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
    // END: 06-08-18


    $("body").on("click", ".header_filter-item", function () {
        if ($(this).children(".header_filter_select").hasClass("open")) {
            $(".header_filter_select").hide().removeClass("open");
            $(this).children(".header_filter-item-wrap").children("img").removeClass("open");
        }
        else {
            $(".header_filter_select").hide().removeClass("open");
            $(".header_filter-item-wrap").children("img").removeClass("open");
            $(this).children(".header_filter_select").show().addClass("open");
            $(this).children(".header_filter-item-wrap").children("img").addClass("open");
        }
    });

    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var select = $(".header_filter-item");
            if (!select.is(e.target) && select.has(e.target).length === 0) {
                $(".header_filter_select").hide().removeClass("open");
                $(".header_filter_select").siblings(".header_filter-item-wrap").children("img").removeClass("open");
            }
        });
    });

    $("body").on("click", ".header-filter_select-wrap ul li", function () {
        $(this).parents(".header_filter-item").children(".header_filter-item-wrap").children("span").text($(this).text());
    });

    $("body").on("click", ".mobile_menu_block-wrap ul li", function () {
        if ($(this).children("ul").length > 0) {
            $(this).siblings().hide();
            $(this).parent("ul").parent("li").children("span").hide();
            $(this).children("ul").show();
            $(this).children("span").addClass("open");
            $(".mobile_menu-back").show();
            return true;
        } else if ($(this).has('a')){
            return true;
        }
        else {
            return false;
        }
    });

    $("body").on("click", ".mobile_menu-back", function () {
        $(".mobile_menu_block-wrap span.open").last().removeClass("open").siblings("ul").hide().parent("li").parent("ul").children("li").show().parent("ul").parent("li").children("span.open").show();
        if (!$(".mobile_menu_block-wrap span.open").length > 0) {
            $(".mobile_menu-back").hide();
            return true;
        }
        else {
            return false;
        }
    });

    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var mobMenu = $(".mobile_menu_block");
            var burger = $(".burger_button");
            if (!mobMenu.is(e.target) && mobMenu.has(e.target).length === 0 && !burger.is(e.target) && burger.has(e.target).length === 0) {
                $(".burger_button-wrap").removeClass("open");
                $(".mobile_menu_block").removeClass("open");
                $(".mobile_menu-overlay").hide();
            }
        });
    });

    $("body").on("click", ".burger_button-wrap", function () {
        if (!$(this).hasClass("open")) {
            $(this).addClass("open");
            $(".mobile_menu_block").addClass("open");
            $(".mobile_menu-overlay").fadeToggle("fast");
        }
        else {
            $(this).removeClass("open");
            $(".mobile_menu_block").removeClass("open");
            $(".mobile_menu-overlay").fadeToggle("fast");
        }
    });
});