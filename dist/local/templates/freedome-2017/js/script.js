$(function(){

	setAffix();	
        
        $("[data-fancybox]").fancybox({
            loop: false,
            protect: true,
            toolbar: true,
            buttons: ['close'],
            margin: [0, 0],
            infobar: true,
            idleTime: 1,
            animationEffect: "zoom-in-out",
            clickContent: false,
            image: {
                preload: false,
            },
            thumbs: {
                autoStart: true
            }
        });
	
});
$(document).on('resize', (function(){
	
	setAffix();
	
}));

$(document).on('affix-bottom.bs.affix',function(event){
	$this = $(event.target);
	$this.find('.offers-vertical').css('padding-bottom','35px');
});

var setAffix = function(){
	
	var top = $('.left-col').offset().top;
	var bottom = 60 + Math.floor((this.bottom = $('.footer').outerHeight(true)) + $('.separate-line').height() + $('.fl-bottom').height()); 
	var rbottom = 47 + bottom;  
	var stopAffix = window.stopAffix || false; 
	
	
	$('.left-affix').affix({
		offset: {
			top: top, 
			bottom: bottom
		}
	});
	
	if ($(window).width() > 1000 && stopAffix !== true) 
	{
		$('.right-navigation, .right-static').affix({
			offset: {
				top: top, 
				bottom: rbottom
			} 
		});
	}
	
};