(function( $ ) {

    $.fn.citrusRealtyAddress = function(options) {
        var self = this,
            opts = $.extend( {}, $.fn.citrusRealtyAddress.defaults, options ),
            $container = $('#' + opts.id);

        this.showMap = function(object) {
            var map = new ymaps.Map(opts.id, {
                center: object.geometry.getCoordinates(),
                zoom: 16,
                controls: opts.controls
            });

            $(window).resize(function () {
                var $innerNodes = $container.find('*');

                $innerNodes.hide();
                map.container.fitToViewport();
                $innerNodes.show();
            });

            if (opts.header)
                object.properties.set('balloonContentHeader', opts.header);
            if (opts.body)
                object.properties.set('balloonContentBody', opts.body);
            if (opts.footer)
                object.properties.set('balloonContentFooter', opts.footer);

            object.options.set('iconLayout', 'default#image');
            object.options.set('iconImageHref', opts.assetsPath + '/application/distr/img/map/' + opts.theme + '_icon.png');
            object.options.set('iconImageSize', [32,52]);
            object.options.set('iconImageOffset', [-16,-48]);

            map.geoObjects.add(object);

            /*map.setBounds(object.geometry.getBounds(), {checkZoomRange: true}).then(function () {
                if (map.getZoom() > 15)
                    map.setZoom(15);
            });*/

            if (opts.openBaloon)
                object.balloon.open();
        };

        ymaps.ready(function () {

            if (opts.coords) {
                self.showMap(new ymaps.Placemark(opts.coords));
            }
            else {
                ymaps.geocode(opts.address, {
                    results: 1
                }).then(function (res) {
                    var object = res.geoObjects.get(0);
                    if (object) {
                        self.showMap(object);
                    }
                    else {
                        if (typeof(opts.onError) === 'function')
                            opts.onError();
                        else {
                            $('.content-map-scale').hide();
                            $container.hide();
                        }
                    }

                }, function (err) {
                    if (typeof(opts.onError) === 'function')
                        opts.onError();
                    else {
                        $('.content-map-scale').hide();
                        $container.hide();
                    }
                });
            }
        });

        return this;
    };

    $.fn.citrusRealtyAddress.defaults = {
        id: '',
        address: '',
        assetsPath: window.citrusTemplatePath,
        theme: window.citrusTemplateTheme,
        header: false,
        body: false,
        footer: false,
        controls: ['smallMapDefaultSet'],
        openBallon: false
    };

}( jQuery ));

$(function () {
    $('.map-link').click(function () {
        var $this = $(this),
            address = $this.data('address'),
            coords = $this.data('coords');

        if (coords || address.length)
	        $.magnificPopup.open({
		        items: {
			        src: '<div class="citrus-objects-map" id="map" style="width: 100%;height: 100%;"></div>',
                },
		        mainClass: 'full-screen-map mfp-with-zoom',
		        alignTop: false,
		        closeOnContentClick: false,
		        callbacks: {
			        open: function () {
				        $().citrusRealtyAddress({
					        id: 'map',
					        address: address,
					        coords: coords,
					        header: '',
					        body: address,
					        footer: '',
					        controls: ['largeMapDefaultSet'],
					        onError: function () {
						        $('#map').height('auto').html('<h2 style="text-align: center">' + BX.message('CITRUS_REALTY_ADDRESS_NOT_FOUND') + '</h2>');
						        $('.fancybox-inner').css('display', 'table-cell').css('vertical-align', 'middle');
						        window.setTimeout($.magnificPopup.close, 2000);
					        }
				        })
			        }
                }
	        });
    });
});
