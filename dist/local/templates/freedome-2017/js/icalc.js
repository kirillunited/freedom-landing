
$(document).ready(function(){
	$(".count_a").click(function(){
		var ID = $(this).attr("id");
		$(".count_div").css('display','none');
		$("#"+ID+"_div").css('display','block');
		$(".count_li_active").css('display','none');
		$(".count_li").css('display','block');
		$("#"+ID+"_li").css('display','none');
		$("#"+ID+"_li_active").css('display','block');
	});
});


// Проверка на заполнение пол\я Город и 
function city()
{
   var df = eval("document.forms[0]");
   if (df.city.selectedIndex == 0)
   {
      alert("Выберите город");
      df.city.focus();
      return false;
   }
   if (df.city.selectedIndex == 1)
   {
      pmin = 120;
      return true;
   }
   if (df.city.selectedIndex == 2)
   {
      pmin = 80;
      return true;
   }
}

// "Простой" ипотечный калькул\ятор
function icalc_simple()
{
   if (city() == false)
   {
      return false;
   }
   var monthly_payment;
   var credit;
   var salary;
   var property_value;
   var initial_instalment;

   var cost_of_money = document.icalc.cost_of_money.value / 100;
   var credit_period = document.icalc.credit_period.value;

   if (!check_item(document.icalc.salary))
   {
      return 0;
   }

   salary = parseInt(document.icalc.salary.value, 10);

   monthly_payment = salary * 0.4;
   credit = monthly_payment / ((cost_of_money / 12) / (1 - Math.pow(1 + (cost_of_money / 12), -(credit_period * 12 - 1))));
   property_value = credit / 0.7;
   initial_instalment = property_value * 0.3;

   document.icalc.salary.value = salary;
   document.icalc.monthly_payment.value = Math.ceil(monthly_payment);
   document.icalc.credit.value = Math.ceil(credit);
   document.icalc.property_value.value = Math.ceil(property_value);
   document.icalc.initial_instalment.value = Math.ceil(initial_instalment);
   return 1;
}


// Расчет по стоимости квартиры
function icalc_flat(ID)
{
   pmin = 120;
   var salary;
   var monthly_payment;
   var credit;
   var property_value;
   var initial_instalment;

   var members = $("#" + ID + "_members").val();
   var cost_of_money = $("#" + ID + "_cost_of_money").val() / 100;
   var credit_period = $("#" + ID + "_credit_period").val();
   var credit_variant = $("#" + ID + "_credit_variant").val();
   var first_value = $("#" + ID + "_first_value").val() / 100;

   property_value = parseInt(getStrNotSpace($("#" + ID + "_property_value").val()), 10);

   salary = Math.max(Math.ceil(monthly_payment) / 0.4, (Math.ceil(monthly_payment) + members * pmin) / 0.6);
   credit = property_value * (1 - first_value);

   monthly_payment = Math.ceil(100 * cost_of_money / 12 * credit / (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12))) / 100;

   q20 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.5;
   q22 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.6;
   if (q22 < 1250)
   {
      if (q20 < 1250)
      {
         salary = q20;
      }
      else
      {
         salary = 1250;
      }
   }
   else
   {
      salary = q22;
   }

   initial_instalment = property_value - credit;

   $("#" + ID + "_property_value").val(getBrokenNumStr(property_value));
   $("#" + ID + "_monthly_payment").val(getBrokenNumStr(monthly_payment));
   $("#" + ID + "_credit").val(getBrokenNumStr(Math.ceil(credit * 100) / 100));
   $("#" + ID + "_salary").val(getBrokenNumStr(Math.ceil(salary * 100) / 100));
   $("#" + ID + "_initial_instalment").val(getBrokenNumStr(initial_instalment));
  
	$("#" + ID + "_div .result").show(500);  
	$('html, body').animate({
		scrollTop: Math.floor( $("#" + ID + "_div .result").offset().top - 100 )  
	}, 500); 
	
   return 1;
}

// Расчет по необходимой зарплате
function icalc_salary(ID)
{
   pmin = 120;
   var salary;
   var monthly_payment;
   var credit;
   var property_value;
   var initial_instalment;

   var members = $("#" + ID + "_members").val();
   var cost_of_money = $("#" + ID + "_cost_of_money").val() / 100;
   var credit_period = $("#" + ID + "_credit_period").val();
   var credit_variant = $("#" + ID + "_credit_variant").val();
   var first_value = $("#" + ID + "_first_value").val() / 100;

   salary = Math.ceil(100 * getStrNotSpace($("#" + ID + "_salary").val() ) ) / 100;

   if (0 == cost_of_money)
   {
      return 0;
   }
   if ((1 + 0.0099 * (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12)) / cost_of_money) == 0)
   {
      return 0;
   }

   if (salary < 1250)
   {
      monthly_payment = (salary * 0.5 - 50) / (1 + 0.0099 * (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12)) / cost_of_money);
   }
   else
   {
      monthly_payment = (salary * 0.6 - 50) / (1 + 0.0099 * (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12)) / cost_of_money);
   }


   if (0 != cost_of_money)
   {
      credit = monthly_payment * 12 * (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12)) / cost_of_money;
   }
   else
   {
      credit = 0;
   }

   //credit = parseInt(document.icalc.credit.value,10);

   if (1 != first_value)
   {
      property_value = credit / (1 - first_value);
   }
   else
   {
      property_value = 0;
   }

   //monthly_payment = Math.ceil(100*cost_of_money/12*credit/(1-Math.pow(1+cost_of_money/12,-credit_period*12)))/100;

   initial_instalment = property_value - credit;

   $("#" + ID + "_property_value").val(getBrokenNumStr(Math.ceil(property_value * 100) / 100));
   $("#" + ID + "_monthly_payment").val(getBrokenNumStr(Math.ceil(100 * monthly_payment) / 100));
   $("#" + ID + "_credit").val(getBrokenNumStr(Math.ceil(credit * 100) / 100));
   $("#" + ID + "_salary").val(getBrokenNumStr(Math.ceil(salary * 100) / 100));
   $("#" + ID + "_initial_instalment").val(getBrokenNumStr(Math.ceil(100 * initial_instalment) / 100));
	
	$("#" + ID + "_div .result").show(500);  
	$('html, body').animate({
		scrollTop: Math.floor( $("#" + ID + "_div .result").offset().top - 100 )  
	}, 500); 
	
   return 1;
}

// Расчет по размеру кредита
function icalc_credit(ID)
{
   pmin = 120;
   var salary;
   var monthly_payment;
   var credit;
   var property_value;
   var initial_instalment;

   var members = $("#" + ID + "_members").val();
   var cost_of_money = $("#" + ID + "_cost_of_money").val() / 100;
   var credit_period = $("#" + ID + "_credit_period").val();
   var credit_variant = $("#" + ID + "_credit_variant").val();
   var first_value = $("#" + ID + "_first_value").val() / 100;

   credit = parseInt(getStrNotSpace($("#" + ID + "_credit").val()), 10);

   if (1 != first_value)
   {
      property_value = credit / (1 - first_value);
   }
   else
   {
      property_value = 0;
   }

   monthly_payment = Math.ceil(100 * cost_of_money / 12 * credit / (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12))) / 100;

   q20 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.5;
   q22 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.6;
   if (q22 < 1250)
   {
      if (q20 < 1250)
      {
         salary = q20;
      }
      else
      {
         salary = 1250;
      }
   }
   else
   {
      salary = q22;
   }

   initial_instalment = property_value - credit;

   $("#" + ID + "_property_value").val(getBrokenNumStr(property_value));
   $("#" + ID + "_monthly_payment").val(getBrokenNumStr(monthly_payment));
   $("#" + ID + "_credit").val(getBrokenNumStr(Math.ceil(credit * 100) / 100));
   $("#" + ID + "_salary").val(getBrokenNumStr(Math.ceil(salary * 100) / 100));
   $("#" + ID + "_initial_instalment").val(getBrokenNumStr(initial_instalment));
	
	$("#" + ID + "_div .result").show(500);  
	$('html, body').animate({
		scrollTop: Math.floor( $("#" + ID + "_div .result").offset().top - 100 )  
	}, 500); 
	
   return 1;
}

// Расчет по ежемес\ячному платежу
function icalc_payment(ID)
{
   pmin = 120;
   var salary;
   var monthly_payment;
   var credit;
   var property_value;
   var initial_instalment;

   var members = $("#" + ID + "_members").val();
   var cost_of_money = $("#" + ID + "_cost_of_money").val() / 100;
   var credit_period = $("#" + ID + "_credit_period").val();
   var credit_variant = $("#" + ID + "_credit_variant").val();
   var first_value = $("#" + ID + "_first_value").val() / 100;

   monthly_payment = Math.ceil(100 * getStrNotSpace($("#" + ID + "_monthly_payment").val()) ) / 100;

   if (0 != cost_of_money)
   {
      credit = monthly_payment * 12 * (1 - Math.pow(1 + cost_of_money / 12, -credit_period * 12)) / cost_of_money;
   }
   else
   {
      credit = 0;
   }

   //credit = parseInt(document.icalc.credit.value,10);

   if (1 != first_value)
   {
      property_value = credit / (1 - first_value);
   }
   else
   {
      property_value = 0;
   }

   //monthly_payment = Math.ceil(100*cost_of_money/12*credit/(1-Math.pow(1+cost_of_money/12,-credit_period*12)))/100;

   q20 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.5;
   q22 = (monthly_payment + 50 + credit * 0.0099 / 12) / 0.6;
   if (q22 < 1250)
   {
      if (q20 < 1250)
      {
         salary = q20;
      }
      else
      {
         salary = 1250;
      }
   }
   else
   {
      salary = q22;
   }

   initial_instalment = property_value - credit;

   $("#" + ID + "_property_value").val(getBrokenNumStr(Math.ceil(property_value * 100) / 100));
   $("#" + ID + "_monthly_payment").val(getBrokenNumStr(monthly_payment));
   $("#" + ID + "_credit").val(getBrokenNumStr(Math.ceil(credit * 100) / 100));
   $("#" + ID + "_salary").val(getBrokenNumStr(Math.ceil(salary * 100) / 100));
   $("#" + ID + "_initial_instalment").val(getBrokenNumStr(Math.ceil(100 * initial_instalment) / 100));
	
	$("#" + ID + "_div .result").show(500);  
	$('html, body').animate({
		scrollTop: Math.floor( $("#" + ID + "_div .result").offset().top - 100 )  
	}, 500); 
	
   return 1;
}

// Расчет по первому взносу
function icalc_initial()
{
   if (city() == false)
   {
      return false;
   }
   var monthly_payment;
   var credit;
   var salary;
   var property_value;
   var initial_instalment;

   var members = document.icalc.members.value;
   var cost_of_money = document.icalc.cost_of_money.value / 100;
   var credit_period = document.icalc.credit_period.value;
   var credit_variant = document.icalc.credit_variant.value;

   if (!check_item(document.icalc.members) || !check_item(document.icalc.initial_instalment))
   {
      return 0;
   }

   initial_instalment = parseInt(document.icalc.initial_instalment.value, 10);

   if (initial_instalment < 15000)
   {

      if (cost_of_money == 0.11 && credit_variant == 1)
         property_value = initial_instalment / 0.3;
      else if (cost_of_money == 0.11 && credit_variant == 2)
         property_value = initial_instalment / 0.237;
      else if (cost_of_money == 0.13 && credit_variant == 1)
         property_value = initial_instalment / 0.3;
      else if (cost_of_money == 0.13 && credit_variant == 2)
         property_value = initial_instalment / 0.265;
      else if (cost_of_money == 0.14 && credit_variant == 1)
         property_value = initial_instalment / 0.3;
      else if (cost_of_money == 0.14 && credit_variant == 2)
         property_value = initial_instalment / 0.2825;
      else
         property_value = initial_instalment / 0.3;

   }
   else
   {

      if (cost_of_money == 0.11 && credit_variant == 1)
         property_value = initial_instalment / 0.2;
      else if (cost_of_money == 0.11 && credit_variant == 2)
         property_value = initial_instalment / 0.137;
      else if (cost_of_money == 0.13 && credit_variant == 1)
         property_value = initial_instalment / 0.2;
      else if (cost_of_money == 0.13 && credit_variant == 2)
         property_value = initial_instalment / 0.165;
      else if (cost_of_money == 0.14 && credit_variant == 1)
         property_value = initial_instalment / 0.2;
      else if (cost_of_money == 0.14 && credit_variant == 2)
         property_value = initial_instalment / 0.1825;
      else
         property_value = initial_instalment / 0.2;

   }

   credit = property_value - initial_instalment;
   monthly_payment = credit * ((cost_of_money / 12) / (1 - Math.pow(1 + (cost_of_money / 12), -(credit_period * 12))));
//         if ((monthly_payment < 316) || (monthly_payment > 840)) salary = Math.max( Math.ceil(monthly_payment)/0.35,(Math.ceil(monthly_payment)+members*pmin)/0.55);
//         else salary = Math.max(Math.ceil(monthly_payment)/0.4,(Math.ceil(monthly_payment)+members*pmin)/0.6);
   salary = Math.max(Math.ceil(monthly_payment) / 0.4, (Math.ceil(monthly_payment) + members * pmin) / 0.6);

   document.icalc.initial_instalment.value = initial_instalment;
   document.icalc.credit.value = Math.ceil(credit);
   document.icalc.salary.value = Math.ceil(salary);
   document.icalc.property_value.value = Math.ceil(property_value);
   document.icalc.monthly_payment.value = Math.ceil(monthly_payment);

   return 1;
}

// Проверка правиьности заполнени\я пол\я
function check_item(item)
{
   var msg = 'Ошибка: указано некорректное (' + item.value + ') ';

   if (item.value == 0 || item.value == '' || !isNumber(item.value))
   {
      if (item.name == 'members')
      {
         msg += 'количество членов семьи';
      }
      else
      {
         msg += 'значение ';

         if (item.name == '')
            msg += '';
         else if (item.name == 'property_value')
            msg += 'стоимости квартиры';
         else if (item.name == 'credit')
            msg += 'размера кредита';
         else if (item.name == 'monthly_payment')
            msg += 'ежемес\ячного платежа';
         else if (item.name == 'salary')
            msg += 'зарплаты после налогообложени\я';
         else if (item.name == 'initial_instalment')
            msg += 'первоначального взноса';
      }
      alert(msg);
      item.value = 0;
      item.focus();
      return 0;
   }

   item.value = parseInt(item.value, 10);

   return 1;
}

// Проверка, \явл\яетс\я ли значение числовым
function isNumber(str)
{
   var numSample = '0123456789';
   var ch;
   var c = 0;
   var i;

   for (i = 0; i < str.length; i++)
   {
      ch = str.substring(i, i + 1);
      if (numSample.indexOf(ch) != -1)
      {
         c++;
      }
   }

   if (c == str.length)
   {
      return(1);
   }

   return(0);
}

function openWin3() {
  myWin= open("", "displayWindow","");

	// сбор данных
	var vid_r = '';
	var vid_v = '';
	var ID = '';
	if($("#count_1_div").css('display') == 'block'){
		vid_r = 'Расчет по квартире';
		vid_v = $("#valuta_1").val();
		ID = 'count_1';
	}else if($("#count_2_div").css('display') == 'block'){
		vid_r = 'Расчет по кредиту';
		vid_v = $("#valuta_2").val();
		ID = 'count_2';
	}else if($("#count_3_div").css('display') == 'block'){
		vid_r = 'Расчет по платежам';
		vid_v = $("#valuta_3").val();
		ID = 'count_3';
	}else if($("#count_4_div").css('display') == 'block'){
		vid_r = 'Расчет по доходам';
		vid_v = $("#valuta_4").val();
		ID = 'count_4';
	}
	
  // открыть объект document для последующей печати
  myWin.document.open();
  
  // генерировать новый документ 
  myWin.document.write("<html><head><title>Ипотечный калькулятор: печатная форма</title>");
  myWin.document.write("<style>body{font-family:arial;margin:50px;}h1{font-size:25px;}");
  myWin.document.write("p{width:400px;border-bottom:1px solid #555;font-size:12px;}");
  myWin.document.write("span{float:right;font-weight:bold;}</style>");
  myWin.document.write("</head><body>"); 
  myWin.document.write("<h1><strong>Ипотечный калькулятор ФРИДОМ:</strong><br /> результаты расчета</h1>");
  myWin.document.write("<p>Вид расчета: <span>"+vid_r+"</span></p>");
  myWin.document.write("<p>Валюта: <span>"+vid_v+"</span></p>");
  
	if($("#count_1_div").css('display') == 'block'){
		myWin.document.write("<p>Процентная ставка (годовая) : <span>"+$("#"+ID+"_cost_of_money").val()+"%</span></p>");
		myWin.document.write("<p>Срок кредита : <span>"+$("#"+ID+"_credit_period").val()+"</span></p>");
		myWin.document.write("<p>Количество членов семьи : <span>"+$("#"+ID+"_members").val()+"</span></p>");
		myWin.document.write("<p>Стоимость квартиры : <span>"+$("#"+ID+"_property_value").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос : <span>"+$("#"+ID+"_first_value").val()+"%</span></p>");
		myWin.document.write("<p>Ежемесячный платеж : <span>"+$("#"+ID+"_monthly_payment").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Доход семьи после налогообложения : <span>"+$("#"+ID+"_salary").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Размер кредита : <span>"+$("#"+ID+"_credit").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос из&nbsp;собственных средств : <span>"+$("#"+ID+"_initial_instalment").val()+" "+vid_v+"</span></p>");
	}else if($("#count_2_div").css('display') == 'block'){
		myWin.document.write("<p>Процентная ставка (годовая) : <span>"+$("#"+ID+"_cost_of_money").val()+"%</span></p>");
		myWin.document.write("<p>Срок кредита : <span>"+$("#"+ID+"_credit_period").val()+"</span></p>");
		myWin.document.write("<p>Количество членов семьи : <span>"+$("#"+ID+"_members").val()+"</span></p>");
		myWin.document.write("<p>Размер кредита : <span>"+$("#"+ID+"_credit").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос : <span>"+$("#"+ID+"_first_value").val()+"%</span></p>");
		myWin.document.write("<p>Ежемесячный платеж : <span>"+$("#"+ID+"_monthly_payment").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Доход семьи после налогообложения : <span>"+$("#"+ID+"_salary").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Стоимость квартиры : <span>"+$("#"+ID+"_property_value").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос из&nbsp;собственных средств : <span>"+$("#"+ID+"_initial_instalment").val()+" "+vid_v+"</span></p>");
	}else if($("#count_3_div").css('display') == 'block'){
		myWin.document.write("<p>Процентная ставка (годовая) : <span>"+$("#"+ID+"_cost_of_money").val()+"%</span></p>");
		myWin.document.write("<p>Срок кредита : <span>"+$("#"+ID+"_credit_period").val()+"</span></p>");
		myWin.document.write("<p>Количество членов семьи : <span>"+$("#"+ID+"_members").val()+"</span></p>");
		myWin.document.write("<p>Ежемесячный платеж : <span>"+$("#"+ID+"_monthly_payment").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос : <span>"+$("#"+ID+"_first_value").val()+"%</span></p>");
		myWin.document.write("<p>Доход семьи после налогообложения : <span>"+$("#"+ID+"_salary").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Стоимость квартиры : <span>"+$("#"+ID+"_property_value").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Размер кредита : <span>"+$("#"+ID+"_credit").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос из&nbsp;собственных средств : <span>"+$("#"+ID+"_initial_instalment").val()+" "+vid_v+"</span></p>");
	}else if($("#count_4_div").css('display') == 'block'){
		myWin.document.write("<p>Процентная ставка (годовая) : <span>"+$("#"+ID+"_cost_of_money").val()+"%</span></p>");
		myWin.document.write("<p>Срок кредита : <span>"+$("#"+ID+"_credit_period").val()+"</span></p>");
		myWin.document.write("<p>Количество членов семьи : <span>"+$("#"+ID+"_members").val()+"</span></p>");
		myWin.document.write("<p>Доход семьи после налогообложения : <span>"+$("#"+ID+"_salary").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос : <span>"+$("#"+ID+"_first_value").val()+"%</span></p>");
		myWin.document.write("<p>Стоимость квартиры : <span>"+$("#"+ID+"_property_value").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Размер кредита : <span>"+$("#"+ID+"_credit").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Первоначальный взнос из&nbsp;собственных средств : <span>"+$("#"+ID+"_initial_instalment").val()+" "+vid_v+"</span></p>");
		myWin.document.write("<p>Ежемесячный платеж : <span>"+$("#"+ID+"_monthly_payment").val()+" "+vid_v+"</span></p>");
	}
  
  
  myWin.document.write("</body></html>");

  // закрыть документ - (но не окно!)
  myWin.document.close();   
}

function getBrokenNumStr(value){
	
	if(typeof value === "number"){
		return (value + "").replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	}
	else if(typeof value === "string"){
		let temp = parseInt(value);
		if(temp)
			return (temp + "").replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		else if(temp === 0)
			return 0;
	}
	return value;
}

function getStrNotSpace(str){
	
	if(typeof str === "string")
		return str.replace(/\s/g, "");
	return str;
}

document.addEventListener("DOMContentLoaded", pageStart);

function pageStart(){
	
	let property_value = document.getElementById("count_1_property_value");
	
	if(property_value){
		property_value.value = getBrokenNumStr(property_value.value);
		
		property_value.addEventListener("keyup", onlyNum);
		property_value.addEventListener("change", onlyNum);
	}
	
	let credit = document.getElementById("count_2_credit");
	
	if(credit){
		credit.value = getBrokenNumStr(credit.value);
		
		credit.addEventListener("keyup", onlyNum);
		credit.addEventListener("change", onlyNum);
	}
	
	let monthly_payment = document.getElementById("count_3_monthly_payment");
	
	if(monthly_payment){
	
		monthly_payment.value = getBrokenNumStr(monthly_payment.value);
		
		monthly_payment.addEventListener("keyup", onlyNum);
		monthly_payment.addEventListener("change", onlyNum);
	}
	
	let salary = document.getElementById("count_4_salary");
	
	if(salary){
		salary.value = getBrokenNumStr(salary.value);
		
		salary.addEventListener("keyup", onlyNum);
		salary.addEventListener("change", onlyNum);
	}
	
	for(let i = 1; i <= 4; i++){
		let credit_period = document.getElementById("count_" + i + "_credit_period");
		if(credit_period){
			credit_period.addEventListener("keyup", onlyNumRange);
			credit_period.addEventListener("change", onlyNumRange);
		}
	}
	
	for(let i = 1; i <= 4; i++){
		let members = document.getElementById("count_" + i + "_members");
		if(members){
			members.addEventListener("keyup", onlyNumRange);
			members.addEventListener("change", onlyNumRange);
		}
	}
	
	for(let i = 1; i <= 4; i++){
		let first_value = document.getElementById("count_" + i + "_first_value");
		if(first_value){
			first_value.addEventListener("keyup", onlyNumRange);
			first_value.addEventListener("change", onlyNumRange);
		}
	}
	
	function onlyNum(event){
	
		event.target.value = event.target.value.replace(/\D/g, "");
		event.target.value = getBrokenNumStr(event.target.value);
	}
	
	function onlyNumRange(event){
	
		event.target.value = event.target.value.replace(/\D/g, "");
		let min = 1;

		if(event.target.id.indexOf("first_value") !== -1)
			min = 0;
			
		let period = parseInt(event.target.value);

		if(period > 100)
			event.target.value = 100;
		else if(period < min)
			event.target.value = min;
	}
}