var citrusSmartFilterNumbers = function (data) {
	var arItem = data,
	    $container = $('.filter-numbers[data-property-id="'+arItem['ID']+'"]'),
		isRangeSlider = arItem['DISPLAY_TYPE'] == 'A',
		$rangeSliderInput = $container.find('.range-slider-input'),
		$minInput = $container.find('.filter-numbers_input._min'),
		$maxInput = $container.find('.filter-numbers_input._max'),
		$numberInputs = $minInput.add($maxInput),
		min = +arItem["VALUES"]["MIN"]["VALUE"],
		max = +arItem["VALUES"]["MAX"]["VALUE"],
        minValue = +( arItem["VALUES"]["MIN"]["HTML_VALUE"] ? arItem["VALUES"]["MIN"]["HTML_VALUE"]: arItem["VALUES"]["MIN"]["VALUE"]),
		maxValue = +( arItem["VALUES"]["MAX"]["HTML_VALUE"] ? arItem["VALUES"]["MAX"]["HTML_VALUE"]: arItem["VALUES"]["MAX"]["VALUE"]),
		sliderStep = +$rangeSliderInput.data('slider-step');

	//doc https://github.com/autoNumeric/autoNumeric
	$numberInputs.autoNumeric('init', {
		digitGroupSeparator: ' ',
		digitalGroupSpacing: '3',
		decimalPlacesOverride: 0,
		minimumValue: 1,
		showWarnings: false
	});
	$numberInputs.attr('data-autonumeric', true);

	if (isRangeSlider) {
		//doc http://ionden.com/a/plugins/ion.rangeslider/demo_advanced.html
		$rangeSliderInput.ionRangeSlider({
			type: "double",
			min: min,
			max: max,
			from: minValue,
			to: maxValue,
			values_separator: ' � ',
			step: sliderStep ? sliderStep : 1,
			onChange: function (data) {
				var from = data.from_pretty == $minInput.data('default-value') ? '' : data.from;
				var to = data.to_pretty == $maxInput.data('default-value') ? '' : data.to;

				$minInput.autoNumeric('set', from);
				$maxInput.autoNumeric('set', to);

				smartFilter.keyup($minInput.get(0));
			}
		});
		var slider = $rangeSliderInput.data('ionRangeSlider');
	}

	$numberInputs.on('keyup', function(event) {
		var from = +$minInput.autoNumeric('get'),
			to = +$maxInput.autoNumeric('get');

		var isValid = function (val) {
			return val >= min && val <= max;
		};

		if (from > max) {$minInput.autoNumeric('set', max); from = max;}
		if (to > max) {$maxInput.autoNumeric('set', max); to = max;}

		if (!isValid(from)) from = min;
		if (!isValid(to)) to = max;

		smartFilter.keyup(this);

		if (isRangeSlider){
			slider.update({
				from: from,
				to: to
			});
		}
	});
};