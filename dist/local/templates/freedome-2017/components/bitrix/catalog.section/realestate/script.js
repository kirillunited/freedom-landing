"use script";

$(document).ready(function(){
	
	$(".modalbox").fancybox();
	
	let new_feedback = document.getElementById('new_feedback');
	new_feedback.addEventListener("submit",sendAjax);
	let aPriceRequestCollection = document.querySelectorAll('a[href="#price-request"]');
	 
	for(let i = 0; i < aPriceRequestCollection.length; i++){
		aPriceRequestCollection[i].addEventListener("click", event => {
		
			event.stopPropagation();
			
			new_feedback.elements.empl.value = event.target.dataset.rieltor_rel;
			new_feedback.elements.lot.value = event.target.dataset.obj_lot;
			new_feedback.elements.object.value = event.target.dataset.object;
		});
	}
});

function sendAjax(event){
	
	event.preventDefault();

	if(checkValidation(event.target)){

		let ajaxPath = event.target.getAttribute("action");
		let xhr = new XMLHttpRequest();
		
		xhr.open("https://www.freedome-realty.ru/baza-nedvizhimosti/zhilaya-nedvizhimost/post", ajaxPath);
		xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
		
		xhr.onload = xhr.onerror = function(){
			if (this.status == 200) {
				$("#new_feedback").fadeOut("fast", function(){
					$(this).before("<p><strong>Ваше сообщение отправлено!</strong></p>");
					setTimeout("$.fancybox.close()", 2000);
				});
			}
			else {
				$("#new_feedback").fadeOut("fast", function(){
					$(this).before("<p><strong>При отправке сообщения произошла ошибка! Попробуйте перезагрузить страницу и отправить сообщение ещё раз.</strong></p>");
					setTimeout("$.fancybox.close()", 4000);
				});
			}
		};
		
		sendingObj = {
			rieltor_rel: event.target.elements.empl.value,
			client: event.target.elements.name.value,
			tel: event.target.elements.phone.value,
			message: event.target.elements.mess.value,
			lot: event.target.elements.lot.value,
			obj: event.target.elements.object.value
		};
		xhr.send(JSON.stringify(sendingObj));
	}
}

function checkValidation(target){
	
	let form = target.closest('form');
	unsetErrorMessage(form);
	
	let errorMessageObj = {
		error: false,
		errorStack: []
	};
	
	let reqInputs = form.querySelectorAll("input[data-required]");
	
	for(let i = 0; i < reqInputs.length; i++){
		
		if(reqInputs[i].name === 'name')
			checkValidationName(reqInputs[i], errorMessageObj);
		else if(reqInputs[i].name === 'phone')
			checkValidationPhone(reqInputs[i], errorMessageObj);
	}
	
	//if(errorMessageObj.error)
	//setErrorMessage(form, errorMessageObj);

	return !errorMessageObj.error;
	
	function checkValidationName(elem, errorMessageObj){
		
		if(elem.value === "" || elem.value.length < 3){
			errorMessageObj.error = true;
			errorMessageObj.errorStack.push("Поле 'Имя пользователя', короче 3 символов");
			setNotValid(elem);
		}
		/*
		if(!errorMessageObj.error){
			
			let match = elem.value.match(/[a-zA-ZА-Яа-я_ёЁ][a-zA-Z0-9А-Яа-яёЁ\s_\-]+/u);
			
			if(!match){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'Имя пользователя', содержит запрещённые символы");
				setNotValid(elem);
			}
			else if(match[0] != elem.value){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'Имя пользователя', содержит запрещённые символы");
				setNotValid(elem);
			}
		}
		*/
		return errorMessageObj.error;
	}
	
	function checkValidationPhone(elem, errorMessageObj){
		
		let match = elem.value.match(/[\+\d\(\)\-][\d\(\)\s\-]{7,}/u);
		
		if(!match){
			errorMessageObj.error = true;
			errorMessageObj.errorStack.push("Поле 'Телефон', содержит запрещённые символы, либо слишком короткое");
			setNotValid(elem);
		}
		else if(match[0] != elem.value){
			errorMessageObj.error = true;
			errorMessageObj.errorStack.push("Поле 'Телефон', содержит запрещённые символы, либо слишком короткое");
			setNotValid(elem);
		}
		return errorMessageObj.error;
	}	
}

function setNotValid(elem){

	if(elem.classList.contains("form-control"))
		elem.classList.add("has-error");
}

function setErrorMessage(form, message){
	
	let errorBlock = document.createElement('ul');
	errorBlock.classList.add("error-message");
	
	if(typeof message == "string"){
		let errorLi = document.createElement('li');
		errorBlock.appendChild(errorLi);
		errorLi.textContent = message;
	}
	else if(typeof message == "object" && message.error){
		
		for(let i = 0; i < message.errorStack.length; i++){
			let errorLi = document.createElement('li');
			errorBlock.appendChild(errorLi);
			errorLi.textContent = message.errorStack[i];
		}
	}
	form.parentElement.insertBefore(errorBlock, form.parentElement.firstChild);
}

function unsetErrorMessage(form){
	
	let OldErrorBlock = form.parentElement.querySelectorAll(".error-message");
	
	for(let i = 0; i < OldErrorBlock.length; i++){
		OldErrorBlock[i].parentElement.removeChild(OldErrorBlock[i]);
	}
	
	let notValid = form.parentElement.querySelectorAll(".has-error");
	
	for(let i = 0; i < notValid.length; i++){
		notValid[i].classList.remove("has-error");
	}
}