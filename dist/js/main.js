$(document).ready(function () {
    // BEGIN: index.js

    // BEGIN: popups
    $('.header_call > a, .footer_application-btn, .btn-action:not(.btn-action--view):not(.btn-action--steps)').magnificPopup({
        closeMarkup: '<button title="%title%" type="button" class="mfp-close popup__close"></button>'
    });
    $('body').on('keyup change blur click', '.input-box[type=text]:not(.input-box[name=comment]), .popup input[type=checkbox]', function () {
        inputValid($(this));
        if ($(this).attr('name') == 'phone') {
            var phone = $(this);
            checkPhone(phone);
        }
        checkForm($(this.form));
    });
    $('body').on('keyup change blur', '.subscribe, .footer_wrap input[type=checkbox]', function () {
        var form = $('#formSub');
        inputValid($(this));
        if ($(this).is('.subscribe')) {
            var elem = $(this);
            checkEmail(elem);
        }
        checkForm($(form));
        return false;
    });
    $('body').on('click', '.filter__item_add .go-to', function () {
        var id = $(this).attr('href');
        $(id).toggleClass('mfp-hide');
        $('.overlay').show();
        return false;
    });
    $('body').on('click', '.filter__item_add > .popup__close', function () {
        $(this).parent().toggleClass('mfp-hide');
        $('.overlay').hide();
        return false;
    });
    $('body').on('click', '.overlay', function () {
        $('#getFilterItemAdd').toggleClass('mfp-hide');
        $('.overlay').hide();
        return false;
    });
    $('body').on('click', '.filter__reset', function () {
        $('.form__reset').click();
    });
    $('form').submit(function () {
        $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
    });
    // END: popups
    // BEGIN: tabs
    $("#tabs, #tabs1").tabs();
    // END: tabs
    // BEGIN: masonary
    masonaryLoad('#tabs-1');
    $('a.tabs-anchor').on('click', function () {
        var elem = $(this).attr('href');
        masonaryLoad(elem);
    });
    // fix masonary adn tabs
    $('.ui-tabs-active').each(function () {
        $(this).on("tabsload", function (event, panel) {
            $('.tabs-panel').masonry({
                itemSelector: '.tabs-panel__item',
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    });
    // END: masonary
    $('body').on('click', '.offers__dropdown', function () {
        $(this).toggleClass('offers__dropdown_open').siblings('.offers__menu').toggleClass('offers__menu_open');
    });
    $(document).click(function (e) {
        if (!$('.offers__dropdown').is(e.target) && !$('.offers__menu').is(e.target) && $('.offers__menu').has(e.target).length === 0) {
            $('.offers__menu').removeClass('offers__menu_open');
            $('.offers__dropdown').removeClass('offers__dropdown_open');
        };
    });
    // contacts
    $('body').on('click', '.card > .card-head', function () {
        $(this)
            .parent()
            .toggleClass('card_open')
            .find('.card-body')
            .toggleClass('card-body_open');
    });
    // END: index.js
    // BEGIN: catalog
    $('body').on('click', '.b-show a.show', function () {
        $(this).addClass('show_load').find('.text span').addClass('loader');
        return false;
    });
    $('body').on('click', '.dropdown__toggle', function () {
        var elem = $(this);
        var parent = elem.parent();
        elem.toggleClass('dropdown__toggle_open').siblings('.dropdown__menu').toggleClass('hidden');
        parent.siblings()
            .find('.dropdown__menu')
            .addClass('hidden')
            .parent()
            .find('.dropdown__toggle')
            .removeClass('dropdown__toggle_open');
        return false;
    });
    $(document).click(function (e) {
        if (!$('.dropdown').is(e.target) && $('.dropdown').has(e.target).length === 0) {
            $('.dropdown__toggle').removeClass('dropdown__toggle_open');
            $('.dropdown__menu').addClass('hidden');
        };
    });
    $('body').on('click', '.dropdown__item', function () {
        var drop = $(this).parent().parent();
        var menu = drop.find('.dropdown__menu');
        var inp = menu.find('input[type=checkbox]');
        var arr = [];
        var arrDefault = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.value').text(arr + " - ");
            if (drop.hasClass('sort__by')) {
                drop.find('.sort__value').text(arr);

            }
        } else if (arr.length == 0) {
            drop.find('.value').text('');
            drop.find('.sort__value').text('умолчанию');
        }
    });
    $('body').on('click', '.form__reset', function () {
        var elem = $(this.form).find('.dropdown__toggle .value');
        $(this.form).find('.tags__item').remove();
        $(elem).each(function () {
            $(this).text('');
        });
    });
    $('body').on('click', '.s-filter__btn, .filter__back', function () {
        $('.filter').toggleClass('filter_hidden');
        return false;
    });
    $(window).resize(function () {
        $('.filter').addClass('filter_hidden');
        $('.ui-tabs-nav li:eq(0) .ui-tabs-anchor').click();
    });
    $('body').on('click', '.tags__item', function () {
        $(this).remove();
        return false;
    });
    $("body").on("keyup", ".filter__item_adress input[type='text']", function () {
        if ($(this).val().length < 1) {
            $(this).parent().find('.dropdown__menu').addClass('hidden');
        } else {
            if ($(this).parent().find('.dropdown__menu').html().length > 1) {
                $(this).parent().find('.dropdown__menu').removeClass('hidden');
            }
        }
    });
    $('body').on('click', '.sort__map', function () {
        $(this).find('span').toggleClass('hidden');
        $('#map').toggleClass('invisible');
    });
    $('body').on('click', '.tags__reset', function () {
        $('.form__reset').click();
        $(this).siblings('.tags__item').remove();
    });
    // END: catalog
    // BEGIN: catalogStart
    if ($(window).width() < 992) {
        $("[id*='tabsCatStart-']").tabs();
    }
    $(window).resize(function () {
        if ($(window).width() < 992) {
            $("[id*='tabsCatStart-']").tabs();
        } else if ($(window).width() >= 992) {
            $("[id*='tabsCatStart-']").tabs('destroy');
            $("[id*='tabCatStart-']").removeAttr('style');
        }
    });

    // END: catalogStart
    // BEGIN: 06-08-18
    $("[id*=navDropTabs-]").tabs();
    // END: 06-08-18
    //anchor price
    $('body').on('click', '.btn-action--view', function () {
        event.preventDefault();
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1000);
    });
    //anchor steps
    $('body').on('click', '.btn-action--steps', function () {
        event.preventDefault();
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 200
        }, 1000);
    });
    //object

    $('body').on('click', '.obj-card_value .dropdown__item', function () {
        var drop = $(this).parent().parent();
        var menu = drop.find('.dropdown__menu');
        var inp = menu.find('input[type=radio]');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.name').text(arr);
        } else if (arr.length == 0) {
            drop.find('.value').text('');
        }
    });
    $('body').on('keyup change blur click', '.input-box[type=text], input[type=checkbox]', function () {
        if ($(this).attr('name') == 'phoneAdvert') {
            var phone = $(this);
            checkPhone(phone);
        }
        checkForm($(this.form));
    });
    //ипотека выпадашка
    $('body').on('click', '.calc .dropdown__item', function () {
        var drop = $(this).parent().parent();
        var menu = drop.find('.dropdown__menu');
        var inp = menu.find('input[type=radio]');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.name').text(arr);
        } else if (arr.length == 0) {
            drop.find('.value').text('');
        }
    });
    //show/hide content
    $('body').on('click', '.infohelp_content .toggle', function () {
        $(this).toggleClass('rotate');

        var tab = $(this).attr('data-target');
        $('[data-attr="' + tab + '"]').slideToggle('slow');
        return false;
    });
});

function checkForm(form) {

    if (!(form.find("input[type=text], input[type=email]").hasClass('error')) && !(form.find("input[type=text], input[type=email]").val() == '') && form.find("input[type=checkbox]").prop("checked") == true) {
        form.find("[type=submit]").addClass("active");
        form.find("[type=submit]").prop("disabled", false);
        return true;
    } else {
        form.find("[type=submit]").removeClass("active");
        form.find("[type=submit]").prop("disabled", true);
    }

    if (!$('.subscribe').hasClass('error') && !$(".subscribe").val() == '' && $('.footer_wrap input[type=checkbox]').prop("checked") == true) {
        form.find("[type=submit]").addClass("active");
        form.find("[type=submit]").prop("disabled", false);
        return true;
    } else {
        form.find("[type=submit]").removeClass("active");
        form.find("[type=submit]").prop("disabled", true);
    }
    return false;
}

function checkEmail(emailItem) {
    var emailItemValue = emailItem.val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{1,3})$/;
    if (!reg.test(emailItemValue)) {
        emailItem.addClass("error");
        // return false;
    } else {
        emailItem.removeClass("error");
        // return true;
    }
}

function masonaryLoad(elem) {
    $(elem).masonry({
        itemSelector: '.tabs-panel__item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });
}

function checkPhone(phone) {
    var phoneVal = phone.val();
    var reg = /^(\s*)?(\+)?([- ():=+]?\d[- ():=+]?){10,14}(\s*)?$/;

    if (!reg.test(phoneVal)) {
        phone.addClass("error");
        return false;
    } else {
        phone.removeClass("error");
        return true;
    }
}
/**
 * add/remove error class for input
 */
function inputValid(elem) {
    if ($(elem).val() == '') {
        $(elem).removeClass('valid');
        $(elem).addClass('error');
    } else {
        $(elem).removeClass('error');
        $(elem).addClass('valid');
    }
}

// //map
// ymaps.ready(init);

// function init() {
//     var myMap = new ymaps.Map("map", {
//             center: [55.765, 37.645],
//             zoom: 15,
//             controls: []
//         }),
//         points = [{
//                 center: [55.76, 37.64],
//                 name: "Офис «Киевская»",
//                 disc: "<p><strong>Режим работы офиса:</strong></p>"
//             },
//             {
//                 center: [55.77, 37.65],
//                 name: "Офис «Киевская»-2",
//                 disc: "<p><strong>Режим работы офиса:</strong></p>"
//             }
//         ];
//     collection = new ymaps.GeoObjectCollection();
// var blocks = $('.card-body-map');
//     for (var i = 0, l = points.length; i < l; i++) {
//         createPointsGroup(points[i], blocks[i]);
//     }

//     function createPointsGroup(item, block) {
//         var submenuItem = $('<a href="javascript:;" class="card-link card-link-map">Показать на карте</a>');
//         // Создаем метку.
//         var placemark = new ymaps.Placemark(item.center, {
//             balloonContentHeader: item.name,
//             balloonContentBody: item.disc
//         }, {
//             iconLayout: 'default#image',
//             iconImageHref: 'assets/img/placemarker.png',
//             iconImageSize: [35, 50]
//         });

//         // Добавляем метку в коллекцию.
//         collection.add(placemark);
//         submenuItem
//         .appendTo($(block))
//         // При клике по пункту подменю открываем/закрываем баллун у метки.
//         // .find('a')
//         .click(function () {
//           if (placemark.balloon.isOpen()) {
//             placemark.balloon.close();
//             return false;
//           } else {
//             myMap.panTo(item.center, {
//               duration: 300
//             }).then(function () {
//               placemark.balloon.open();
//             });
//           }
//         });
//     }
//     myMap.geoObjects.add(collection);
// }