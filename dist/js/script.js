$(function () {
    $('body').on('click', '.js-anchor', function (event) {
        event.preventDefault();

        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1000);
    });

    //form valid
    $(".main-wrapper form").submit(function (event) {
        event.preventDefault();

        var form = $(this);
        var inp = form.find('[data-required]');

        $.each(inp, function () {
            if ($(this).is('[type=tel]')) {
                checkPhoneMain($(this));
            }
            if ($(this).is('[type=checkbox]')) {
                if (!($(this).prop('checked') == true)) {
                    $(this).parent().addClass('is-error');
                } else {
                    $(this).parent().removeClass('is-error');
                }
            }
        });

        if (form.find('.is-error').length) {
            return false;
        };

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            complete: function (response) {
                console.log('success');
            }
        });
    });

    /**slider init func */
    initAddSectionSlider($('.js-add-section-slider'));
});

function checkPhoneMain(phone) {
    var phoneVal = phone.val();
    var reg = /^(\s*)?(\+)?([- ():=+]?\d[- ():=+]?){10,14}(\s*)?$/;

    if (!reg.test(phoneVal)) {
        phone.parent().addClass("is-error");
        return false;
    } else {
        phone.parent().removeClass("is-error");
    }
}

/**
 * add-section slider
 * @param slidesToShow - кол-во слайдов для показа
 */
function initAddSectionSlider(elem) {
    var sliderList = $(elem),
        slidesToShow = 2;

    if (sliderList && !sliderList.hasClass("slick-initialized")) {
        sliderList.each(function () {
            $(this).slick({
                infinite: true,
                slidesToShow: slidesToShow,
                slidesToScroll: 1,
                centerMode: false,
                autoplay: true,
                dots: true,
                arrows: false,
                adaptiveHeight: true,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    }
}